// alert('hi')
//request server, load all info that include in your request -> galing sa api, return a format of json
// fetch('url', options)
	//url- which the request is to be made
	//options- an array of properties, optional parameter (optional if we use methods(post, put), default behavior is to retrieve)

//showPost
//response .json should be in json format
//anonymous func, you can call parameter anything
fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json()).then(data => showPosts(data))//showPosts function

const showPosts = (posts) => {

	//like schema; assign data to post entries variable
	let postEntries = "";
		//process each index from placeholder
		posts.forEach(post => {
			console.log(post)

			postEntries +=  `
   								<div id='post-${post.id}'>
								<h3 id='post-title-${post.id}'>${post.title}</h3>
								<p id='post-body-${post.id}'>${post.body}</p>

								<button onclick='editPost(${post.id})'>Edit</button>

								<button onclick='deletePost(${post.id})'>Delete</button>

							</div>`
		})

		document.querySelector('#div-post-entries').innerHTML = postEntries;

};

//ADD POST; target where add post happen
//triggering event then the function
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	 	e.preventDefault();

	 	fetch('https://jsonplaceholder.typicode.com/posts', {

	 		method: 'POST',
	 		body: JSON.stringify({
	 			title: document.querySelector("#txt-title").value,
	 			body: document.querySelector("#txt-body").value,
	 			userId: 1
	 		}),
	 		headers: {
	 			'Content-Type' : 'application/json'
	 		}
	 	})
	 	.then(response => response.json()).then(data => {
	 		console.log(data)
	 		alert('Added Successfully');

	 		//reset that state of input into null/blank after submitting new post
	 	document.querySelector('#txt-title').value = null;
	 	document.querySelector('#txt-body').value = null;
	 	})

});

//edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

	document.querySelector("#btn-submit-update").removeAttribute('disabled');
}

//update post

document.querySelector("#form-edit-post").addEventListener('submit', (e) => {

	e.preventDefault()

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => response.json()).then(data => {
		console.log(data)
		alert('Successfully updated')


		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
})


//delete Post


const deletePost = (id) => {

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {

		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json',
		},
	})

	document.querySelector(`#post-${id}`).remove();


}
